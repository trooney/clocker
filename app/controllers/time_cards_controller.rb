class TimeCardsController < ApplicationController
  before_action :set_time_card, only: [:show, :edit, :update, :destroy]

  def index
    @time_cards = TimeCard.includes(:teacher).all
  end

  def edit
  end

  def update
    if @time_card.update(time_card_params)
      redirect_to time_cards_path, notice: 'Time card was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @time_card.destroy
    redirect_to time_cards_path, notice: 'Time card was successfully destroyed.'
  end

  # Use callbacks to share common setup or constraints between actions.
  private def set_time_card
    @time_card = TimeCard.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  private def time_card_params
    params.require(:time_card).permit(:started_at, :ended_at, :teacher)
  end
end
