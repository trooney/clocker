class ApplicationController < ActionController::Base
  before_action :check_if_active_time_card

  # Fetching records on every request leads to poor performance. But good enough for this prototype.
  def check_if_active_time_card
    if TimeCard.active.exists?
      flash[:info] = "You're already clocked in. Clock out to use other features."
      redirect_to ticker_time_clock_index_path
    end
  end
end
