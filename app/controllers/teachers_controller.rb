class TeachersController < ApplicationController
  def create
    teacher = Teacher.find_by(name: teacher_params[:name])

    if teacher
      redirect_to clock_in_time_clock_index_path({ teacher_id: teacher.id })
      return
    end

    teacher = Teacher.new(teacher_params)

    if teacher.save
      redirect_to clock_in_time_clock_index_path({ teacher_id: teacher.id })
      return
    end

    flash[:error] = teacher.errors.full_messages.join(' ')
    redirect_to root_path
  end

  def teacher_params
    params.require(:teacher).permit(:name)
  end
end
