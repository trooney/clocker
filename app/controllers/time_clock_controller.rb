class TimeClockController < ApplicationController
  # Skip before action can lead to spaghetti callbacks. However in a small prototype
  # this can quickly implement global behaviour.
  skip_before_action :check_if_active_time_card, only: [:ticker, :clock_out]

  def index
    @teacher = Teacher.new

    # Provided simple bounds. Never return "all results". Pagination
    # and a flexible limit should be used. Saving time by setting a fixed limit.
    @teachers = Teacher.all.order(name: :asc).limit(50)
  end

  def clock_in
    teacher = Teacher.find(params[:teacher_id])

    TimeCard.create!(teacher: teacher, started_at: Time.now)

    redirect_to ticker_time_clock_index_path
  end

  def ticker
    @time_card = TimeCard.includes(:teacher).active.first
  end

  def clock_out
    time_card = TimeCard.find(params[:time_card_id])
    time_card.update!(ended_at: Time.now)

    redirect_to root_path
  end
end
