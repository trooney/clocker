class Teacher < ApplicationRecord
  # My form can references these constants in order to limit the length of the input.
  MIN_NAME_LENGTH = 3
  MAX_NAME_LENGTH = 50

  has_many :time_cards

  validates :name, presence: true, uniqueness: true, length: { minimum: MIN_NAME_LENGTH, maximum: MAX_NAME_LENGTH }
end
