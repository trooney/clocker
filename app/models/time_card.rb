class TimeCard < ApplicationRecord
  belongs_to :teacher, optional: false

  validates :started_at, presence: true
  validate :end_date_after_start_date, if: -> { ended_at.present? }

  delegate :name, to: :teacher, prefix: true

  scope :active, -> {
    where(ended_at: nil)
      .order(id: :desc)
  }

  private def end_date_after_start_date
    if ended_at < started_at
      errors.add :end_date, 'must be after start date'
    end
  end
end
