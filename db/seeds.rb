require 'faker'

Teacher.delete_all
TimeCard.delete_all


teachers = [
  Teacher.create!(name: Faker::Name.name),
  Teacher.create!(name: Faker::Name.name),
  Teacher.create!(name: Faker::Name.name)
]


TimeCard.create!(teacher: teachers[0], started_at: Time.now - 4.days, ended_at: Time.now - 3.days)
TimeCard.create!(teacher: teachers[1], started_at: Time.now - 3.days, ended_at: Time.now - 2.days)
TimeCard.create!(teacher: teachers[2], started_at: Time.now - 2.days, ended_at: Time.now - 1.days)
