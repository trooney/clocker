class CreateTimeCards < ActiveRecord::Migration[6.0]
  def change
    create_table :time_cards do |t|
      t.datetime :started_at
      t.datetime :ended_at
      t.references :teacher

      t.timestamps
    end
  end
end
