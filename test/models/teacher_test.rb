require 'test_helper'

class TeacherTest < ActiveSupport::TestCase
  def setup
    @model = Teacher.new
  end

  def test_associations
    assert_must have_many(:time_cards), @model
  end

  def test_validations
    assert_must validate_presence_of(:name), @model
    assert_must validate_uniqueness_of(:name), @model
    assert_must validate_length_of(:name).is_at_least(3).is_at_most(50), @model
  end
end
