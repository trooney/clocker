require 'test_helper'

class TimeCardTest < ActiveSupport::TestCase
  def setup
    @model = TimeCard.new(teacher: Teacher.new(name: 'foo'))
  end

  def test_associations
    assert_must belong_to(:teacher), @model
  end

  def test_validations
    assert_must validate_presence_of(:started_at), @model
  end

  def test_scopes_but_just_that_they_exist
    assert_respond_to TimeCard, :active

  end

  def test_teacher_name
    assert_equal @model.teacher.name, @model.teacher_name
  end
end
