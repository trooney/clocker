require 'test_helper'

class TeachersControllerTest < ActionDispatch::IntegrationTest
  test 'should create a teacher' do
    assert_difference -> { Teacher.count } do
      post teachers_url params: { teacher: { name: 'foobar' } }
    end

    assert_redirected_to clock_in_time_clock_index_path(teacher_id: Teacher.last.id)
  end

  test 'should handle existing teachers' do
    existing_teacher = Teacher.create!(name: 'foobar')

    assert_no_difference -> { Teacher.count } do
      post teachers_url params: { teacher: { name: existing_teacher.name } }
    end

    assert_redirected_to clock_in_time_clock_index_path(teacher_id: Teacher.last.id)
  end
end
