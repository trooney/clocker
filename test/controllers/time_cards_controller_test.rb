require 'test_helper'

class TimeCardsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @time_card = time_cards(:one)
  end

  test 'should get index' do
    get time_cards_url
    assert_response :success
  end

  test 'should get edit' do
    get edit_time_card_url(@time_card)
    assert_response :success
  end

  test 'should update time_card' do
    patch time_card_url(@time_card), params: { time_card: { ended_at: @time_card.ended_at, started_at: @time_card.started_at, teacher_id: @time_card.teacher_id } }
    assert_redirected_to time_cards_url
  end

  test 'should destroy time_card' do
    assert_difference('TimeCard.count', -1) do
      delete time_card_url(@time_card)
    end

    assert_redirected_to time_cards_url
  end
end
