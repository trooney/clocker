require 'test_helper'

class TimeClockControllerTest < ActionDispatch::IntegrationTest
  def setup
    @teacher = teachers(:one)
  end

  test 'clocks in a teacher' do
    assert_difference -> { TimeCard.where(teacher_id: @teacher.id).count } do
      get clock_in_time_clock_index_path, params: { teacher_id: @teacher.id }
    end

    assert_redirected_to ticker_time_clock_index_path
  end

  test 'clocks out a teacher' do
    started_at = Time.now
    time_card = TimeCard.create!(teacher: @teacher, started_at: started_at)

    get clock_out_time_clock_index_path, params: { time_card_id: time_card.id }

    time_card.reload

    assert_not_nil time_card.ended_at

    assert_redirected_to root_path
  end

end
