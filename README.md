# Clocker

Built for HiMama: [https://dry-bayou-44889.herokuapp.com/](https://dry-bayou-44889.herokuapp.com/)

### Tasks

- [X] Allow users to enter name
- [X] Clock user in & out of app 
- [X] Allow multiple clock in/outs per day
- [X] Show all clock events
- [X] Update & destroy clock events

### Summary

A user is shown a list of teachers. They find their name, and press clock-in. When finished, they press clock-out. If not on the list, they can add themselves. New teachers are automatically clocked-in. Also, you can use the "Time Cards" link view all time cards and modify or delete them. 

_ Notes: _ The requirements says "allow a user to enter their name or log in". Since there's no mention of passwords or logging out, I assume we primarily want the ability to add new teachers.

#### How did you approach this challenge?

This challenge is a screening tool. It's requirements are small. So my goal was to finish the task, and not to over think, over design, or over complicate things. Rails basic MVC structure, server side forms, and Bootstrap via CDN are all _good enough_. 

However I did try to flesh out happy path testing and validation to avoid bad data. The models have validation and mild test coverage. There are reasonable controllers, and while I made no effort to "slim" them, they are well named and their methods tested. The various forms also implement validation and display at least some error message when things go wrong.

#### What schema design did you choose and why?

The simplest design. It tracks all required information. 

I imagine there's one other idea. A table with and "type" and "time" columns. You'd have one entry for type "CLOCK_IN" and another for "CLOCK_OUT". This would have some faster queries, since you could index the "TYPE" field. But you'd always be dependent on sorting the items and figuring out how to pair up records to create a whole "clock in-out" cycle.    

There are likely lots of issues relating to people who have clocked in but not clocked out, and also with modifying the in/out timestamps. If I clock in today at 9AM, and it's 3PM, I'm probably "on the clock". But if I go back a week and remove my "clock out" time -- am I still clocked in today? Or does the clock-in period assumed to be terminated, say, that day at night, or within 24 hours? I can imagine lots of edge cases. 

#### If you were given another day to work on this, how would you spend it?

I viewed this app as a kind of thing sitting on someone's desk. A single user tool. I did realize the main screen has a list with "clock in" buttons. I don't need the "holding" page with the clock out button. Just that list with clock in/out buttons. That's allow multiple people to use the same app.

Other than that, I would try and add a full acceptance test to click through the entire system. And if I had a little time I would format the dates better.

#### Another month?

Depends on the needs... maybe... 

- Add in support for time zones
- Implement pagination, query optimizations and what not
- Have some nice auto-complete search boxes (because we now use pagination)
- Add authentication mechanisms and a kind of "school" or "community" or "group" and attach teachers to those items
- Add in some kind of ACL system to figure out what people can view what things
- Probably have some kind of weekly or monthly report (people like charts)
- Maybe just an export to CSV so they can make their own charts
- Setup webpacker and probably use some react for auto-complete boxes and other widgets
- Use docker or ansible to have a predictable environment
- Stripe integration: offer 15 days free access, then start charging (gotta pay those AWS costs, right?)


 