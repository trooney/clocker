Rails.application.routes.draw do
  root to: 'time_clock#index'

  resources :teachers, only: [:create]
  resources :time_cards
  resources :time_clock do
    collection do
      get :clock_in
      get :clock_out
      get :ticker
    end
  end
end
